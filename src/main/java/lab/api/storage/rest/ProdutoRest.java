package lab.api.storage.rest;

import lab.api.storage.domain.Produto;
import lab.api.storage.repository.ProdutoRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/produtos")
@AllArgsConstructor
public class ProdutoRest {

    private ProdutoRepository produtoRepository;

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") Long id) {
        var produtoOptional = produtoRepository.findById(id);
        if (produtoOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(produtoOptional.get());
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Produto não encontrado\n");
    }

    @GetMapping
    public ResponseEntity<?> findAll() {
        var listProducts = produtoRepository.findAll();
        if (!listProducts.isEmpty()) {
            return ResponseEntity.status(HttpStatus.OK).body(listProducts);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Não há produtos no sistema\n");
    }

    @PostMapping
    public ResponseEntity<Produto> insert(@RequestBody Produto produto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(produtoRepository.save(produto));
    }

    @PostMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Produto produtoAtualizado) {
        var produto = produtoRepository.findById(id);
        if (produto.isPresent()) {
            BeanUtils.copyProperties(produtoAtualizado, produto.get());
            return ResponseEntity.status(HttpStatus.OK).body(produtoRepository.save(produto.get()));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Produto não Encontrado\n");
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        var produto = produtoRepository.findById(id);
        if (produto.isPresent()) {
            produtoRepository.deleteById(id);
            return ResponseEntity.status(HttpStatus.OK).body("Produto Deletado\n");
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Produto não Encontrado\n");
    }
}
