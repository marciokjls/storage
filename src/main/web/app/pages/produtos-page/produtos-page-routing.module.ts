import {NgModule} from '@angular/core';
import {Routes} from '@angular/router';
import {DatagridComponent} from "./components/datagrid/datagrid.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home'
  },
  {
    path: '**',
    redirectTo: 'home'
  }
];

@NgModule({
  imports: [],
  declarations: [DatagridComponent]
})
export class AppRoutingModule { }
