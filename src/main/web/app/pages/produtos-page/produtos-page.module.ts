import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DatagridComponent } from './components/datagrid/datagrid.component';

@NgModule({
  declarations: [
    DatagridComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ProdutosPageModule { }
